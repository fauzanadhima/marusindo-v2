import FormProcessor from './form-processor';

jQuery(window).on('elementor/frontend/init', () => {
	elementorFrontend.hooks.addAction('frontend/element_ready/text-editor.default', el => {
		const forms = jQuery(el).find('.kaliforms-form-container');
		Array.prototype.forEach.call(forms, e => {
			new FormProcessor(e)
		})
	});
	elementorFrontend.hooks.addAction('frontend/element_ready/shortcode.default', el => {
		const forms = jQuery(el).find('.kaliforms-form-container');
		Array.prototype.forEach.call(forms, e => {
			new FormProcessor(e)
		})
	});
	elementorFrontend.hooks.addAction('frontend/element_ready/kaliforms.default', el => {
		const forms = jQuery(el).find('.kaliforms-form-container');
		Array.prototype.forEach.call(forms, e => {
			new FormProcessor(e)
		})
	});
});
