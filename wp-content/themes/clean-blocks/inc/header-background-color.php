<?php
/**
 * Customizer functionality
 *
 * @package Clean_Blocks
 */

/**
 * Sets up the WordPress core custom header and custom background features.
 *
 * @since Clean Blocks 1.0
 *
 * @see clean_blocks_header_style()
 */
function clean_blocks_custom_header_and_background() {
	$default_background_color = '#f3f5f5';
	$default_text_color       = '#000000';

	/**
	 * Filter the arguments used when adding 'custom-header' support in Persona.
	 *
	 * @since Clean Blocks 1.0
	 *
	 * @param array $args {
	 *     An array of custom-header support arguments.
	 *
	 *     @type string $default-text-color Default color of the header text.
	 *     @type int      $width            Width in pixels of the custom header image. Default 1200.
	 *     @type int      $height           Height in pixels of the custom header image. Default 280.
	 *     @type bool     $flex-height      Whether to allow flexible-height header images. Default true.
	 *     @type callable $wp-head-callback Callback function used to style the header image and text
	 *                                      displayed on the blog.
	 * }
	 */
	add_theme_support( 'custom-header', apply_filters( 'clean_blocks_custom_header_args', array(
		'default-image'      	 => get_parent_theme_file_uri( '/assets/images/header-image.jpg' ),
		'default-text-color'     => $default_text_color,
		'width'                  => 1920,
		'height'                 => 822,
		'flex-height'            => true,
		'flex-height'            => true,
		'wp-head-callback'       => 'clean_blocks_header_style',
		'video'                  => true,
	) ) );

	register_default_headers( array(
		'default-image' => array(
			'url'           => '%s/assets/images/header-image.jpg',
			'thumbnail_url' => '%s/assets/images/header-image-275x155.jpg',
			'description'   => esc_html__( 'Default Header Image', 'clean-blocks' ),
		),
	) );
}
add_action( 'after_setup_theme', 'clean_blocks_custom_header_and_background' );

/**
 * Customize video play/pause button in the custom header.
 *
 * @param array $settings header video settings.
 */
function clean_blocks_video_controls( $settings ) {
	$settings['l10n']['play'] = '<span class="screen-reader-text">' . esc_html__( 'Play background video', 'clean-blocks' ) . '</span>' . clean_blocks_get_svg( array(
		'icon' => 'play',
	) );
	$settings['l10n']['pause'] = '<span class="screen-reader-text">' . esc_html__( 'Pause background video', 'clean-blocks' ) . '</span>' . clean_blocks_get_svg( array(
		'icon' => 'pause',
	) );

	return $settings;
}
add_filter( 'header_video_settings', 'clean_blocks_video_controls' );

/**
 * Binds the JS listener to make Customizer color_scheme control.
 *
 * Passes color scheme data as colorScheme global.
 *
 * @since Clean Blocks 1.0
 */
function clean_blocks_customize_control_js() {
	/*section Sorter css*/
	wp_enqueue_style( 'clean-blocks-custom-controls-css', trailingslashit( esc_url( get_template_directory_uri() ) ) . 'assets/css/customizer.css' );
}
add_action( 'customize_controls_enqueue_scripts', 'clean_blocks_customize_control_js' );