<?php
/**
 * Featured Slider Options
 *
 * @package Clean_Blocks
 */

/**
 * Add hero content options to theme options
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function clean_blocks_slider_options( $wp_customize ) {
	$wp_customize->add_section( 'clean_blocks_featured_slider', array(
			'panel' => 'clean_blocks_theme_options',
			'title' => esc_html__( 'Featured Slider', 'clean-blocks' ),
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_slider_option',
			'default'           => 'disabled',
			'sanitize_callback' => 'clean_blocks_sanitize_select',
			'choices'           => clean_blocks_section_visibility_options(),
			'label'             => esc_html__( 'Enable on', 'clean-blocks' ),
			'section'           => 'clean_blocks_featured_slider',
			'type'              => 'select',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_slider_number',
			'default'           => '4',
			'sanitize_callback' => 'clean_blocks_sanitize_number_range',

			'active_callback'   => 'clean_blocks_is_slider_active',
			'description'       => esc_html__( 'Save and refresh the page if No. of Slides is changed (Max no of slides is 20)', 'clean-blocks' ),
			'input_attrs'       => array(
				'style' => 'width: 100px;',
				'min'   => 0,
				'max'   => 20,
				'step'  => 1,
			),
			'label'             => esc_html__( 'No of Slides', 'clean-blocks' ),
			'section'           => 'clean_blocks_featured_slider',
			'type'              => 'number',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_slider_content_show',
			'default'           => 'hide-content',
			'sanitize_callback' => 'clean_blocks_sanitize_select',
			'active_callback'   => 'clean_blocks_is_slider_active',
			'choices'           => clean_blocks_content_show(),
			'label'             => esc_html__( 'Display Content', 'clean-blocks' ),
			'section'           => 'clean_blocks_featured_slider',
			'type'              => 'select',
		)
	);

	$slider_number = get_theme_mod( 'clean_blocks_slider_number', 4 );

	for ( $i = 1; $i <= $slider_number ; $i++ ) {
		// Page Sliders
		clean_blocks_register_option( $wp_customize, array(
				'name'              =>'clean_blocks_slider_page_' . $i,
				'sanitize_callback' => 'clean_blocks_sanitize_post',
				'active_callback'   => 'clean_blocks_is_slider_active',
				'label'             => esc_html__( 'Page', 'clean-blocks' ) . ' # ' . $i,
				'section'           => 'clean_blocks_featured_slider',
				'type'              => 'dropdown-pages',
			)
		);
	} // End for().
}
add_action( 'customize_register', 'clean_blocks_slider_options' );

/**
 * Returns an array of featured content show registered
 *
 * @since Clean Blocks 1.0
 */
function clean_blocks_content_show() {
	$options = array(
		'excerpt'      => esc_html__( 'Show Excerpt', 'clean-blocks' ),
		'full-content' => esc_html__( 'Full Content', 'clean-blocks' ),
		'hide-content' => esc_html__( 'Hide Content', 'clean-blocks' ),
	);
	return apply_filters( 'clean_blocks_content_show', $options );
}


/**
 * Returns an array of featured content show registered
 *
 * @since Clean Blocks 1.0
 */
function clean_blocks_meta_show() {
	$options = array(
		'show-meta'      => esc_html__( 'Show Meta', 'clean-blocks' ),
		'hide-meta' => esc_html__( 'Hide Meta', 'clean-blocks' ),
	);
	return apply_filters( 'clean_blocks_meta_show', $options );
}

/** Active Callback Functions */

if( ! function_exists( 'clean_blocks_is_slider_active' ) ) :
	/**
	* Return true if slider is active
	*
	* @since Clean Blocks 1.0
	*/
	function clean_blocks_is_slider_active( $control ) {
		$enable = $control->manager->get_setting( 'clean_blocks_slider_option' )->value();

		return ( clean_blocks_check_section( $enable ) );
	}
endif;