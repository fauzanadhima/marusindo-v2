<?php
/**
* The template for adding Service Settings in Customizer
*
 * @package Clean_Blocks
*/

function clean_blocks_service_options( $wp_customize ) {
	// Add note to Jetpack Portfolio Section
    clean_blocks_register_option( $wp_customize, array(
            'name'              => 'clean_blocks_jetpack_portfolio_cpt_note',
            'sanitize_callback' => 'sanitize_text_field',
            'custom_control'    => 'Clean_Blocks_Note_Control',
            'label'             => sprintf( esc_html__( 'For Service Options for Clean Blocks Theme, go %1$shere%2$s', 'clean-blocks' ),
                 '<a href="javascript:wp.customize.section( \'clean_blocks_service\' ).focus();">',
                 '</a>'
            ),
            'section'           => 'ect_service',
            'type'              => 'description',
            'priority'          => 1,
        )
    );

	$wp_customize->add_section( 'clean_blocks_service', array(
			'panel'    => 'clean_blocks_theme_options',
			'title'    => esc_html__( 'Service', 'clean-blocks' ),
		)
	);

	$action = 'install-plugin';
    $slug   = 'essential-content-types';

    $install_url = wp_nonce_url(
        add_query_arg(
            array(
                'action' => $action,
                'plugin' => $slug
            ),
            admin_url( 'update.php' )
        ),
        $action . '_' . $slug
    );

    clean_blocks_register_option( $wp_customize, array(
            'name'              => 'clean_blocks_service_jetpack_note',
            'sanitize_callback' => 'sanitize_text_field',
            'custom_control'    => 'Clean_Blocks_Note_Control',
            'active_callback'   => 'clean_blocks_is_ect_services_inactive',
            /* translators: 1: <a>/link tag start, 2: </a>/link tag close. */
            'label'             => sprintf( esc_html__( 'For Services, install %1$sEssential Content Types%2$s Plugin with Service Type Enabled', 'clean-blocks' ),
                '<a target="_blank" href="' . esc_url( $install_url ) . '">',
                '</a>'

            ),
           'section'            => 'clean_blocks_service',
            'type'              => 'description',
            'priority'          => 1,
        )
    );

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_service_option',
			'default'           => 'disabled',
			'sanitize_callback' => 'clean_blocks_sanitize_select',
			'active_callback'   => 'clean_blocks_is_ect_services_active',
			'choices'           => clean_blocks_section_visibility_options(),
			'label'             => esc_html__( 'Enable on', 'clean-blocks' ),
			'section'           => 'clean_blocks_service',
			'type'              => 'select',
		)
	);

    clean_blocks_register_option( $wp_customize, array(
            'name'              => 'clean_blocks_service_cpt_note',
            'sanitize_callback' => 'sanitize_text_field',
            'custom_control'    => 'Clean_Blocks_Note_Control',
            'active_callback'   => 'clean_blocks_is_service_active',
            /* translators: 1: <a>/link tag start, 2: </a>/link tag close. */
			'label'             => sprintf( esc_html__( 'For CPT heading and sub-heading, go %1$shere%2$s', 'clean-blocks' ),
                 '<a href="javascript:wp.customize.control( \'ect_service_title\' ).focus();">',
                 '</a>'
            ),
            'section'           => 'clean_blocks_service',
            'type'              => 'description',
        )
    );

	clean_blocks_register_option( $wp_customize, array(
				'name'              => 'clean_blocks_service_number',
				'default'           => 6,
				'sanitize_callback' => 'clean_blocks_sanitize_number_range',
				'active_callback'   => 'clean_blocks_is_service_active',
				'description'       => esc_html__( 'Save and refresh the page if No. of Service is changed', 'clean-blocks' ),
				'input_attrs'       => array(
					'style' => 'width: 100px;',
					'min'   => 0,
				),
				'label'             => esc_html__( 'No of Service', 'clean-blocks' ),
				'section'           => 'clean_blocks_service',
				'type'              => 'number',
		)
	);

	$number = get_theme_mod( 'clean_blocks_service_number', 6 );

	for ( $i = 1; $i <= $number ; $i++ ) {
		//for CPT
		clean_blocks_register_option( $wp_customize, array(
				'name'              => 'clean_blocks_service_cpt_' . $i,
				'sanitize_callback' => 'clean_blocks_sanitize_post',
				'default'           => 0,
				'active_callback'   => 'clean_blocks_is_service_active',
				'label'             => esc_html__( 'Service ', 'clean-blocks' ) . ' ' . $i ,
				'section'           => 'clean_blocks_service',
				'type'              => 'select',
				'choices'           => clean_blocks_generate_post_array( 'ect-service' ),
			)
		);
	} // End for().
}
add_action( 'customize_register', 'clean_blocks_service_options' );

if ( ! function_exists( 'clean_blocks_is_service_active' ) ) :
	/**
	* Return true if service is active
	*
	* @since Clean Blocks 1.0
	*/
	function clean_blocks_is_service_active( $control ) {
		$enable = $control->manager->get_setting( 'clean_blocks_service_option' )->value();

		//return true only if previwed page on customizer matches the type of content option selected
		return ( clean_blocks_is_ect_services_active( $control ) &&  clean_blocks_check_section( $enable ) );
	}
endif;

if ( ! function_exists( 'clean_blocks_is_ect_services_inactive' ) ) :
    /**
    * Return true if service is active
    *
    * @since Clean Blocks 1.0
    */
    function clean_blocks_is_ect_services_inactive( $control ) {
        return ! ( class_exists( 'Essential_Content_Service' ) || class_exists( 'Essential_Content_Pro_Service' ) );
    }
endif;

if ( ! function_exists( 'clean_blocks_is_ect_services_active' ) ) :
    /**
    * Return true if service is active
    *
    * @since Clean Blocks 1.0
    */
    function clean_blocks_is_ect_services_active( $control ) {
        return ( class_exists( 'Essential_Content_Service' ) || class_exists( 'Essential_Content_Pro_Service' ) );
    }
endif;
