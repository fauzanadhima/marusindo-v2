<?php
/**
 * Add Portfolio Settings in Customizer
 *
 * @package Clean_Blocks
 */

/**
 * Add portfolio options to theme options
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function clean_blocks_portfolio_options( $wp_customize ) {
    // Add note to Jetpack Portfolio Section
    clean_blocks_register_option( $wp_customize, array(
            'name'              => 'clean_blocks_jetpack_portfolio_cpt_note',
            'sanitize_callback' => 'sanitize_text_field',
            'custom_control'    => 'Clean_Blocks_Note_Control',
            'label'             => sprintf( esc_html__( 'For Portfolio Options for Clean Blocks Theme, go %1$shere%2$s', 'clean-blocks' ),
                 '<a href="javascript:wp.customize.section( \'clean_blocks_portfolio\' ).focus();">',
                 '</a>'
            ),
            'section'           => 'jetpack_portfolio',
            'type'              => 'description',
            'priority'          => 1,
        )
    );

	$wp_customize->add_section( 'clean_blocks_portfolio', array(
            'panel'    => 'clean_blocks_theme_options',
            'title'    => esc_html__( 'Portfolio', 'clean-blocks' ),
        )
    );

    $action = 'install-plugin';
    $slug   = 'essential-content-types';

    $install_url = wp_nonce_url(
        add_query_arg(
            array(
                'action' => $action,
                'plugin' => $slug
            ),
            admin_url( 'update.php' )
        ),
        $action . '_' . $slug
    );

    clean_blocks_register_option( $wp_customize, array(
            'name'              => 'clean_blocks_portfolio_jetpack_note',
            'sanitize_callback' => 'sanitize_text_field',
            'custom_control'    => 'Clean_Blocks_Note_Control',
            'active_callback'   => 'clean_blocks_is_ect_portfolio_inactive',
            /* translators: 1: <a>/link tag start, 2: </a>/link tag close. */
            'label'             => sprintf( esc_html__( 'For Portfolio, install %1$sEssential Content Types%2$s Plugin with Portfolio Type Enabled', 'clean-blocks' ),
                '<a target="_blank" href="' . esc_url( $install_url ) . '">',
                '</a>'

            ),
           'section'            => 'clean_blocks_portfolio',
            'type'              => 'description',
            'priority'          => 1,
        )
    );

    clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_portfolio_option',
			'default'           => 'disabled',
			'sanitize_callback' => 'clean_blocks_sanitize_select',
            'active_callback'   => 'clean_blocks_is_ect_portfolio_active',
			'choices'           => clean_blocks_section_visibility_options(),
			'label'             => esc_html__( 'Enable on', 'clean-blocks' ),
			'section'           => 'clean_blocks_portfolio',
			'type'              => 'select',
		)
	);

    clean_blocks_register_option( $wp_customize, array(
            'name'              => 'clean_blocks_portfolio_cpt_note',
            'sanitize_callback' => 'sanitize_text_field',
            'custom_control'    => 'Clean_Blocks_Note_Control',
            'active_callback'   => 'clean_blocks_is_portfolio_active',
            /* translators: 1: <a>/link tag start, 2: </a>/link tag close. */
			'label'             => sprintf( esc_html__( 'For CPT heading and sub-heading, go %1$shere%2$s', 'clean-blocks' ),
                 '<a href="javascript:wp.customize.control( \'jetpack_portfolio_title\' ).focus();">',
                 '</a>'
            ),
            'section'           => 'clean_blocks_portfolio',
            'type'              => 'description',
        )
    );

    clean_blocks_register_option( $wp_customize, array(
            'name'              => 'clean_blocks_portfolio_number',
            'default'           => '6',
            'sanitize_callback' => 'clean_blocks_sanitize_number_range',
            'active_callback'   => 'clean_blocks_is_portfolio_active',
            'label'             => esc_html__( 'Number of items to show', 'clean-blocks' ),
            'section'           => 'clean_blocks_portfolio',
            'type'              => 'number',
            'input_attrs'       => array(
                'style' => 'width: 100px;',
                'min'   => 0,
            ),
        )
    );

    $number = get_theme_mod( 'clean_blocks_portfolio_number', 6 );

    for ( $i = 1; $i <= $number ; $i++ ) {
        //for CPT
        clean_blocks_register_option( $wp_customize, array(
                'name'              => 'clean_blocks_portfolio_cpt_' . $i,
                'sanitize_callback' => 'clean_blocks_sanitize_post',
                'active_callback'   => 'clean_blocks_is_portfolio_active',
                'label'             => esc_html__( 'Portfolio', 'clean-blocks' ) . ' ' . $i ,
                'section'           => 'clean_blocks_portfolio',
                'type'              => 'select',
                'choices'           => clean_blocks_generate_post_array( 'jetpack-portfolio' ),
            )
        );

    } // End for().
}
add_action( 'customize_register', 'clean_blocks_portfolio_options' );

/**
 * Active Callback Functions
 */
if ( ! function_exists( 'clean_blocks_is_portfolio_active' ) ) :
    /**
    * Return true if portfolio is active
    *
    * @since Clean Blocks 1.0
    */
    function clean_blocks_is_portfolio_active( $control ) {
        $enable = $control->manager->get_setting( 'clean_blocks_portfolio_option' )->value();

        //return true only if previwed page on customizer matches the type of content option selected
        return ( clean_blocks_is_ect_portfolio_active( $control ) &&  clean_blocks_check_section( $enable ) );
    }
endif;

if ( ! function_exists( 'clean_blocks_is_ect_portfolio_inactive' ) ) :
    /**
    *
    * @since Clean Blocks 1.0
    */
    function clean_blocks_is_ect_portfolio_inactive( $control ) {
        return ! ( class_exists( 'Essential_Content_Jetpack_Portfolio' ) || class_exists( 'Essential_Content_Pro_Jetpack_Portfolio' ) );
    }
endif;

if ( ! function_exists( 'clean_blocks_is_ect_portfolio_active' ) ) :
    /**
    *
    * @since Clean Blocks 1.0
    */
    function clean_blocks_is_ect_portfolio_active( $control ) {
        return ( class_exists( 'Essential_Content_Jetpack_Portfolio' ) || class_exists( 'Essential_Content_Pro_Jetpack_Portfolio' ) );
    }
endif;