<?php
/**
 * Team options
 *
 * @package Clean_Blocks
 */

/**
 * Add team content options to theme options
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function clean_blocks_team_options( $wp_customize ) {

	$wp_customize->add_section( 'clean_blocks_team', array(
			'title' => esc_html__( 'Team', 'clean-blocks' ),
			'panel' => 'clean_blocks_theme_options',
		)
	);

	// Add color scheme setting and control.
	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_team_option',
			'default'           => 'disabled',
			'sanitize_callback' => 'clean_blocks_sanitize_select',
			'choices'           => clean_blocks_section_visibility_options(),
			'label'             => esc_html__( 'Enable on', 'clean-blocks' ),
			'section'           => 'clean_blocks_team',
			'type'              => 'select',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_team_layout',
			'default'           => 'layout-three',
			'sanitize_callback' => 'clean_blocks_sanitize_select',
			'active_callback'   => 'clean_blocks_is_team_active',
			'choices'           => clean_blocks_sections_layout_options(),
			'label'             => esc_html__( 'Select Featured Content Layout', 'clean-blocks' ),
			'section'           => 'clean_blocks_team',
			'type'              => 'select',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_team_title',
			'default'           => esc_html__( 'Our Team', 'clean-blocks' ),
			'sanitize_callback' => 'wp_kses_post',
			'active_callback'   => 'clean_blocks_is_team_active',
			'label'             => esc_html__( 'Headline', 'clean-blocks' ),
			'section'           => 'clean_blocks_team',
			'type'              => 'text',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_team_sub_title',
			'sanitize_callback' => 'wp_kses_post',
			'active_callback'   => 'clean_blocks_is_team_active',
			'label'             => esc_html__( 'Sub headline', 'clean-blocks' ),
			'section'           => 'clean_blocks_team',
			'type'              => 'textarea',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_team_type',
			'default'           => 'category',
			'sanitize_callback' => 'clean_blocks_sanitize_select',
			'active_callback'   => 'clean_blocks_is_team_active',
			'choices'           => clean_blocks_section_type_options(),
			'label'             => esc_html__( 'Type', 'clean-blocks' ),
			'section'           => 'clean_blocks_team',
			'type'              => 'select',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_team_number',
			'default'           => 3,
			'sanitize_callback' => 'clean_blocks_sanitize_number_range',
			'active_callback'   => 'clean_blocks_is_team_active',
			'description'       => esc_html__( 'Save and refresh the page if No. of Items is changed', 'clean-blocks' ),
			'input_attrs'       => array(
				'style' => 'width: 100px;',
				'min'   => 0,
			),
			'label'             => esc_html__( 'No of Items', 'clean-blocks' ),
			'section'           => 'clean_blocks_team',
			'type'              => 'number',
			'transport'         => 'postMessage',
		)
	);

	$content_display = clean_blocks_content_show();

	unset( $content_display['full-content'] ) ;

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_team_show',
			'default'           => 'hide-content',
			'sanitize_callback' => 'clean_blocks_sanitize_select',
			'active_callback'   => 'clean_blocks_is_team_post_page_category_content_active',
			'choices'           => clean_blocks_content_show(),
			'label'             => esc_html__( 'Display Content', 'clean-blocks' ),
			'section'           => 'clean_blocks_team',
			'type'              => 'select',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_team_meta_show',
			'default'           => 'hide-meta',
			'sanitize_callback' => 'clean_blocks_sanitize_select',
			'active_callback'   => 'clean_blocks_is_team_post_page_category_content_active',
			'choices'           => clean_blocks_meta_show(),
			'label'             => esc_html__( 'Display Meta', 'clean-blocks' ),
			'section'           => 'clean_blocks_team',
			'type'              => 'select',
		)
	);


	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_team_select_category',
			'sanitize_callback' => 'clean_blocks_sanitize_category_list',
			'custom_control'    => 'Clean_Blocks_Multi_Cat_Control',
			'active_callback'   => 'clean_blocks_is_team_category_content_active',
			'label'             => esc_html__( 'Select Categories', 'clean-blocks' ),
			'name'              => 'clean_blocks_team_select_category',
			'section'           => 'clean_blocks_team',
			'type'              => 'dropdown-categories',
		)
	);

	$number = get_theme_mod( 'clean_blocks_team_number', 3 );

	//loop for team post content
	for ( $i = 1; $i <= $number ; $i++ ) {
		clean_blocks_register_option( $wp_customize, array(
				'name'              => 'clean_blocks_team_post_' . $i,
				'sanitize_callback' => 'clean_blocks_sanitize_post',
				'default'           => 0,
				'active_callback'   => 'clean_blocks_is_team_post_content_active',
				'input_attrs'       => array(
					'style'             => 'width: 40px;'
				),
				'label'             => esc_html__( 'Post', 'clean-blocks' ) . ' ' . $i ,
				'section'           => 'clean_blocks_team',
				'choices'           => clean_blocks_generate_post_array(),
				'type'              => 'select'
			)
		);

		clean_blocks_register_option( $wp_customize, array(
				'name'              => 'clean_blocks_team_page_' . $i,
				'sanitize_callback' => 'clean_blocks_sanitize_post',
				'active_callback'   => 'clean_blocks_is_team_page_content_active',
				'label'             => esc_html__( 'Team Page', 'clean-blocks' ) . ' ' . $i ,
				'section'           => 'clean_blocks_team',
				'type'              => 'dropdown-pages',
			)
		);

		clean_blocks_register_option( $wp_customize, array(
				'name'              => 'clean_blocks_team_note_' . $i,
				'sanitize_callback' => 'sanitize_text_field',
				'custom_control'    => 'Clean_Blocks_Note_Control',
				'active_callback'   => 'clean_blocks_is_team_image_content_active',
				'label'             => esc_html__( 'Team #', 'clean-blocks' ) .  $i,
				'section'           => 'clean_blocks_team',
				'type'              => 'description',
			)
		);

		clean_blocks_register_option( $wp_customize, array(
				'name'              => 'clean_blocks_team_image_' . $i,
				'sanitize_callback' => 'clean_blocks_sanitize_image',
				'custom_control'    => 'WP_Customize_Image_Control',
				'active_callback'   => 'clean_blocks_is_team_image_content_active',
				'label'             => esc_html__( 'Image', 'clean-blocks' ),
				'section'           => 'clean_blocks_team',
			)
		);

		clean_blocks_register_option( $wp_customize, array(
				'name'              => 'clean_blocks_team_link_' . $i,
				'sanitize_callback' => 'esc_url_raw',
				'active_callback'   => 'clean_blocks_is_team_image_content_active',
				'label'             => esc_html__( 'Link', 'clean-blocks' ),
				'section'           => 'clean_blocks_team',
			)
		);

		clean_blocks_register_option( $wp_customize, array(
				'name'              => 'clean_blocks_team_target_' . $i,
				'sanitize_callback' => 'clean_blocks_sanitize_checkbox',
				'active_callback'   => 'clean_blocks_is_team_image_content_active',
				'label'             => esc_html__( 'Open Link in New Window/Tab', 'clean-blocks' ),
				'section'           => 'clean_blocks_team',
				'custom_control'    => 'Clean_Blocks_Toggle_Control',
			)
		);

		clean_blocks_register_option( $wp_customize, array(
				'name'              => 'clean_blocks_team_title_' . $i,
				'sanitize_callback' => 'sanitize_text_field',
				'active_callback'   => 'clean_blocks_is_team_image_content_active',
				'label'             => esc_html__( 'Title', 'clean-blocks' ),
				'section'           => 'clean_blocks_team',
				'type'              => 'text',
			)
		);

		clean_blocks_register_option( $wp_customize, array(
				'name'              => 'clean_blocks_team_position_' . $i,
				'sanitize_callback' => 'sanitize_text_field',
				'active_callback'   => 'clean_blocks_is_team_image_content_active',
				'label'             => esc_html__( 'Position', 'clean-blocks' ),
				'section'           => 'clean_blocks_team',
				'type'              => 'text',
			)
		);

		clean_blocks_register_option( $wp_customize, array(
				'name'              => 'clean_blocks_team_content_' . $i,
				'sanitize_callback' => 'wp_kses_post',
				'active_callback'   => 'clean_blocks_is_team_image_content_active',
				'label'             => esc_html__( 'Content', 'clean-blocks' ),
				'section'           => 'clean_blocks_team',
				'type'              => 'textarea',
			)
		);
		clean_blocks_register_option( $wp_customize, array(
				'name'              => 'clean_blocks_team_social_link_one_' . $i,
				'sanitize_callback' => 'esc_url_raw',
				'active_callback'   => 'clean_blocks_is_team_active',
				'label'             => esc_html__( 'Team #', 'clean-blocks' ) .  $i . esc_html__( ': Social Link #1', 'clean-blocks' ),
				'section'           => 'clean_blocks_team',
			)
		);

		clean_blocks_register_option( $wp_customize, array(
				'name'              => 'clean_blocks_team_social_link_two_' . $i,
				'sanitize_callback' => 'esc_url_raw',
				'active_callback'   => 'clean_blocks_is_team_active',
				'label'             => esc_html__( 'Team #', 'clean-blocks' ) .  $i . esc_html__( ': Social Link #2', 'clean-blocks' ),
				'section'           => 'clean_blocks_team',
			)
		);

		clean_blocks_register_option( $wp_customize, array(
				'name'              => 'clean_blocks_team_social_link_three_' . $i,
				'sanitize_callback' => 'esc_url_raw',
				'active_callback'   => 'clean_blocks_is_team_active',
				'label'             => esc_html__( 'Team #', 'clean-blocks' ) .  $i . esc_html__( ': Social Link #3', 'clean-blocks' ),
				'section'           => 'clean_blocks_team',
			)
		);

		clean_blocks_register_option( $wp_customize, array(
				'name'              => 'clean_blocks_team_social_link_four_' . $i,
				'sanitize_callback' => 'esc_url_raw',
				'active_callback'   => 'clean_blocks_is_team_active',
				'label'             => esc_html__( 'Team #', 'clean-blocks' ) .  $i . esc_html__( ': Social Link #4', 'clean-blocks' ),
				'section'           => 'clean_blocks_team',
			)
		);
	} // End for().

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_team_text',
			'default'           => esc_html__( 'View All', 'clean-blocks' ),
			'sanitize_callback' => 'sanitize_text_field',
			'active_callback'   => 'clean_blocks_is_team_active',
			'label'             => esc_html__( 'Button Text', 'clean-blocks' ),
			'section'           => 'clean_blocks_team',
			'type'              => 'text',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_team_link',
			'sanitize_callback' => 'esc_url_raw',
			'active_callback'   => 'clean_blocks_is_team_active',
			'label'             => esc_html__( 'Button Link', 'clean-blocks' ),
			'section'           => 'clean_blocks_team',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_team_target',
			'sanitize_callback' => 'clean_blocks_sanitize_checkbox',
			'active_callback'   => 'clean_blocks_is_team_active',
			'label'             => esc_html__( 'Open Link in New Window/Tab', 'clean-blocks' ),
			'section'           => 'clean_blocks_team',
			'custom_control'    => 'Clean_Blocks_Toggle_Control',
		)
	);
}
add_action( 'customize_register', 'clean_blocks_team_options', 10 );

/** Active Callback Functions **/
if ( ! function_exists( 'clean_blocks_is_team_active' ) ) :
	/**
	* Return true if team content is active
	*
	* @since Clean Blocks 1.0
	*/
	function clean_blocks_is_team_active( $control ) {
		$enable = $control->manager->get_setting( 'clean_blocks_team_option' )->value();

		//return true only if previewed page on customizer matches the type of content option selected
		return ( clean_blocks_check_section( $enable ) );
	}
endif;

if ( ! function_exists( 'clean_blocks_is_team_post_content_active' ) ) :
	/**
	* Return true if page content is active
	*
	* @since Clean Blocks 1.0
	*/
	function clean_blocks_is_team_post_content_active( $control ) {
		$type = $control->manager->get_setting( 'clean_blocks_team_type' )->value();

		//return true only if previewed page on customizer matches the type of content option selected and is or is not selected type
		return ( clean_blocks_is_team_active( $control ) && ( 'post' === $type ) );
	}
endif;

if ( ! function_exists( 'clean_blocks_is_team_page_content_active' ) ) :
	/**
	* Return true if page content is active
	*
	* @since Clean Blocks 1.0
	*/
	function clean_blocks_is_team_page_content_active( $control ) {
		$type = $control->manager->get_setting( 'clean_blocks_team_type' )->value();

		//return true only if previewed page on customizer matches the type of content option selected and is or is not selected type
		return ( clean_blocks_is_team_active( $control ) && ( 'page' === $type ) );
	}
endif;

if ( ! function_exists( 'clean_blocks_is_team_category_content_active' ) ) :
	/**
	* Return true if page content is active
	*
	* @since Clean Blocks 1.0
	*/
	function clean_blocks_is_team_category_content_active( $control ) {
		$type = $control->manager->get_setting( 'clean_blocks_team_type' )->value();

		//return true only if previewed page on customizer matches the type of content option selected and is or is not selected type
		return ( clean_blocks_is_team_active( $control ) && ( 'category' === $type ) );
	}
endif;

if ( ! function_exists( 'clean_blocks_is_team_post_page_category_content_active' ) ) :
	/**
	* Return true if page content is active
	*
	* @since Clean Blocks 1.0
	*/
	function clean_blocks_is_team_post_page_category_content_active( $control ) {
		$type = $control->manager->get_setting( 'clean_blocks_team_type' )->value();

		//return true only if previewed page on customizer matches the type of content option selected and is or is not selected type
		return ( clean_blocks_is_team_active( $control ) && ( 'category' === $type || 'page' === $type || 'post' === $type ) );
	}
endif;

if ( ! function_exists( 'clean_blocks_is_team_image_content_active' ) ) :
	/**
	* Return true if page content is active
	*
	* @since Clean Blocks 1.0
	*/
	function clean_blocks_is_team_image_content_active( $control ) {
		$type = $control->manager->get_setting( 'clean_blocks_team_type' )->value();

		//return true only if previewed page on customizer matches the type of content option selected and is or is not selected type
		return ( clean_blocks_is_team_active( $control ) && ( 'custom' === $type ) );
	}
endif;
