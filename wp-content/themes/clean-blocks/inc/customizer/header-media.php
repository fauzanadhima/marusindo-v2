<?php
/**
 * Header Media Options
 *
 * @package Clean_Blocks
 */

function clean_blocks_header_media_options( $wp_customize ) {
$wp_customize->get_section( 'header_image' )->description = esc_html__( 'If you add video, it will only show up on Homepage/FrontPage. Other Pages will use Header/Post/Page Image depending on your selection of option. Header Image will be used as a fallback while the video loads ', 'clean-blocks' );

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_header_media_option',
			'default'           => 'homepage',
			'sanitize_callback' => 'clean_blocks_sanitize_select',
			'choices'           => array(
				'homepage'               => esc_html__( 'Homepage / Frontpage', 'clean-blocks' ),
				'exclude-home'           => esc_html__( 'Excluding Homepage', 'clean-blocks' ),
				'exclude-home-page-post' => esc_html__( 'Excluding Homepage, Page/Post Featured Image', 'clean-blocks' ),
				'entire-site'            => esc_html__( 'Entire Site', 'clean-blocks' ),
				'entire-site-page-post'  => esc_html__( 'Entire Site, Page/Post Featured Image', 'clean-blocks' ),
				'pages-posts'            => esc_html__( 'Pages and Posts', 'clean-blocks' ),
				'disable'                => esc_html__( 'Disabled', 'clean-blocks' ),
			),
			'label'             => esc_html__( 'Enable on', 'clean-blocks' ),
			'section'           => 'header_image',
			'type'              => 'select',
			'priority'          => 1,
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_header_media_subtitle',
			'default'           => esc_html__( 'Crypto Currencies', 'clean-blocks' ),
			'sanitize_callback' => 'wp_kses_post',
			'label'             => esc_html__( 'Header Media Sub Title', 'clean-blocks' ),
			'section'           => 'header_image',
			'type'              => 'text',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_header_media_title',
			'default'           => esc_html__( 'Absolute Power', 'clean-blocks' ),
			'sanitize_callback' => 'wp_kses_post',
			'label'             => esc_html__( 'Header Media Title', 'clean-blocks' ),
			'section'           => 'header_image',
			'type'              => 'text',
		)
	);

    clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_header_media_text',
			'default'           => esc_html__( 'Make things as simple as possible but no simpler.', 'clean-blocks' ),
			'sanitize_callback' => 'wp_kses_post',
			'label'             => esc_html__( 'Header Media Text', 'clean-blocks' ),
			'section'           => 'header_image',
			'type'              => 'textarea',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_header_media_url',
			'default'           => '#',
			'sanitize_callback' => 'esc_url_raw',
			'label'             => esc_html__( 'Header Media Url', 'clean-blocks' ),
			'section'           => 'header_image',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_header_media_url_text',
			'default'           => esc_html__( 'Continue Reading', 'clean-blocks' ),
			'sanitize_callback' => 'sanitize_text_field',
			'label'             => esc_html__( 'Header Media Url Text', 'clean-blocks' ),
			'section'           => 'header_image',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_header_url_target',
			'sanitize_callback' => 'clean_blocks_sanitize_checkbox',
			'label'             => esc_html__( 'Open Link in New Window/Tab', 'clean-blocks' ),
			'section'           => 'header_image',
			'custom_control'    => 'Clean_Blocks_Toggle_Control',
		)
	);
}
add_action( 'customize_register', 'clean_blocks_header_media_options' );
