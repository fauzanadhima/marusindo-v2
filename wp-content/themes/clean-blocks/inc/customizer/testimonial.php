<?php
/**
 * Add Testimonial Settings in Customizer
 *
 * @package Clean_Blocks
*/

/**
 * Add testimonial options to theme options
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function clean_blocks_testimonial_options( $wp_customize ) {
    // Add note to Jetpack Testimonial Section
    clean_blocks_register_option( $wp_customize, array(
            'name'              => 'clean_blocks_jetpack_testimonial_cpt_note',
            'sanitize_callback' => 'sanitize_text_field',
            'custom_control'    => 'Clean_Blocks_Note_Control',
            'label'             => sprintf( esc_html__( 'For Testimonial Options for Clean Blocks Theme, go %1$shere%2$s', 'clean-blocks' ),
                '<a href="javascript:wp.customize.section( \'clean_blocks_testimonials\' ).focus();">',
                 '</a>'
            ),
           'section'            => 'jetpack_testimonials',
            'type'              => 'description',
            'priority'          => 1,
        )
    );

    $wp_customize->add_section( 'clean_blocks_testimonials', array(
            'panel'    => 'clean_blocks_theme_options',
            'title'    => esc_html__( 'Testimonials', 'clean-blocks' ),
        )
    );

    $action = 'install-plugin';
    $slug   = 'essential-content-types';

    $install_url = wp_nonce_url(
        add_query_arg(
            array(
                'action' => $action,
                'plugin' => $slug
            ),
            admin_url( 'update.php' )
        ),
        $action . '_' . $slug
    );

    clean_blocks_register_option( $wp_customize, array(
            'name'              => 'clean_blocks_testimonial_jetpack_note',
            'sanitize_callback' => 'sanitize_text_field',
            'custom_control'    => 'Clean_Blocks_Note_Control',
            'active_callback'   => 'clean_blocks_is_ect_testimonial_inactive',
            /* translators: 1: <a>/link tag start, 2: </a>/link tag close. */
            'label'             => sprintf( esc_html__( 'For Testimonial, install %1$sEssential Content Types%2$s Plugin with testimonial Type Enabled', 'clean-blocks' ),
                '<a target="_blank" href="' . esc_url( $install_url ) . '">',
                '</a>'

            ),
           'section'            => 'clean_blocks_testimonials',
            'type'              => 'description',
            'priority'          => 1,
        )
    );

    clean_blocks_register_option( $wp_customize, array(
            'name'              => 'clean_blocks_testimonial_option',
            'default'           => 'disabled',
            'sanitize_callback' => 'clean_blocks_sanitize_select',
            'active_callback'   => 'clean_blocks_is_ect_testimonial_active',
            'choices'           => clean_blocks_section_visibility_options(),
            'label'             => esc_html__( 'Enable on', 'clean-blocks' ),
            'section'           => 'clean_blocks_testimonials',
            'type'              => 'select',
            'priority'          => 1,
        )
    );

    $layouts = clean_blocks_sections_layout_options();

    unset( $layouts['layout-three'], $layouts['layout-four'] );

    clean_blocks_register_option( $wp_customize, array(
            'name'              => 'clean_blocks_testimonial_cpt_note',
            'sanitize_callback' => 'sanitize_text_field',
            'custom_control'    => 'Clean_Blocks_Note_Control',
            'active_callback'   => 'clean_blocks_is_testimonial_active',
            /* translators: 1: <a>/link tag start, 2: </a>/link tag close. */
			'label'             => sprintf( esc_html__( 'For CPT heading and sub-heading, go %1$shere%2$s', 'clean-blocks' ),
                '<a href="javascript:wp.customize.section( \'jetpack_testimonials\' ).focus();">',
                '</a>'
            ),
            'section'           => 'clean_blocks_testimonials',
            'type'              => 'description',
        )
    );

    clean_blocks_register_option( $wp_customize, array(
            'name'              => 'clean_blocks_testimonial_number',
            'default'           => '3',
            'sanitize_callback' => 'clean_blocks_sanitize_number_range',
            'active_callback'   => 'clean_blocks_is_testimonial_active',
            'label'             => esc_html__( 'Number of items to show', 'clean-blocks' ),
            'section'           => 'clean_blocks_testimonials',
            'type'              => 'number',
            'input_attrs'       => array(
                'style'             => 'width: 100px;',
                'min'               => 0,
            ),
        )
    );

    $number = get_theme_mod( 'clean_blocks_testimonial_number', 3 );

    for ( $i = 1; $i <= $number ; $i++ ) {
        //for CPT
        clean_blocks_register_option( $wp_customize, array(
                'name'              => 'clean_blocks_testimonial_cpt_' . $i,
                'sanitize_callback' => 'clean_blocks_sanitize_post',
                'active_callback'   => 'clean_blocks_is_testimonial_active',
                'label'             => esc_html__( 'Testimonial', 'clean-blocks' ) . ' ' . $i ,
                'section'           => 'clean_blocks_testimonials',
                'type'              => 'select',
                'choices'           => clean_blocks_generate_post_array( 'jetpack-testimonial' ),
            )
        );
    } // End for().
}
add_action( 'customize_register', 'clean_blocks_testimonial_options' );

/**
 * Active Callback Functions
 */
if ( ! function_exists( 'clean_blocks_is_testimonial_active' ) ) :
    /**
    * Return true if testimonial is active
    *
    * @since Clean Blocks 1.0
    */
    function clean_blocks_is_testimonial_active( $control ) {
        $enable = $control->manager->get_setting( 'clean_blocks_testimonial_option' )->value();

        //return true only if previwed page on customizer matches the type of content option selected
        return ( clean_blocks_is_ect_testimonial_active( $control ) &&  clean_blocks_check_section( $enable ) );
    }
endif;

if ( ! function_exists( 'clean_blocks_is_ect_testimonial_inactive' ) ) :
    /**
    *
    * @since Clean Blocks 1.0
    */
    function clean_blocks_is_ect_testimonial_inactive( $control ) {
        return ! ( class_exists( 'Essential_Content_Jetpack_testimonial' ) || class_exists( 'Essential_Content_Pro_Jetpack_testimonial' ) );
    }
endif;

if ( ! function_exists( 'clean_blocks_is_ect_testimonial_active' ) ) :
    /**
    *
    * @since Clean Blocks 1.0
    */
    function clean_blocks_is_ect_testimonial_active( $control ) {
        return ( class_exists( 'Essential_Content_Jetpack_testimonial' ) || class_exists( 'Essential_Content_Pro_Jetpack_testimonial' ) );
    }
endif;


