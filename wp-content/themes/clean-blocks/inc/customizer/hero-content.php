<?php
/**
 * Hero Content Options
 *
 * @package Clean_Blocks
 */

/**
 * Add hero content options to theme options
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function clean_blocks_hero_content_options( $wp_customize ) {
	$wp_customize->add_section( 'clean_blocks_hero_content_options', array(
			'title' => esc_html__( 'Hero Content Options', 'clean-blocks' ),
			'panel' => 'clean_blocks_theme_options',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_hero_content_visibility',
			'default'           => 'disabled',
			'sanitize_callback' => 'clean_blocks_sanitize_select',
			'choices'           => clean_blocks_section_visibility_options(),
			'label'             => esc_html__( 'Enable on', 'clean-blocks' ),
			'section'           => 'clean_blocks_hero_content_options',
			'type'              => 'select',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_hero_content',
			'default'           => '0',
			'sanitize_callback' => 'clean_blocks_sanitize_post',
			'active_callback'   => 'clean_blocks_is_hero_content_active',
			'label'             => esc_html__( 'Page', 'clean-blocks' ),
			'section'           => 'clean_blocks_hero_content_options',
			'type'              => 'dropdown-pages',
		)
	);
}
add_action( 'customize_register', 'clean_blocks_hero_content_options' );

/** Active Callback Functions **/
if ( ! function_exists( 'clean_blocks_is_hero_content_active' ) ) :
	/**
	* Return true if hero content is active
	*
	* @since Clean Blocks 1.0
	*/
	function clean_blocks_is_hero_content_active( $control ) {
		$enable = $control->manager->get_setting( 'clean_blocks_hero_content_visibility' )->value();

		return ( clean_blocks_check_section( $enable ) );
	}
endif;