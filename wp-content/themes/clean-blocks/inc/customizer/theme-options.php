<?php
/**
 * Theme Options
 *
 * @package Clean_Blocks
 */

/**
 * Add theme options
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function clean_blocks_theme_options( $wp_customize ) {
	$wp_customize->add_panel( 'clean_blocks_theme_options', array(
		'title'    => esc_html__( 'Theme Options', 'clean-blocks' ),
		'priority' => 130,
	) );

	// Layout Options
	$wp_customize->add_section( 'clean_blocks_layout_options', array(
		'title' => esc_html__( 'Layout Options', 'clean-blocks' ),
		'panel' => 'clean_blocks_theme_options',
		)
	);

	/* Default Layout */
	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_default_layout',
			'default'           => 'right-sidebar',
			'sanitize_callback' => 'clean_blocks_sanitize_select',
			'label'             => esc_html__( 'Default Layout', 'clean-blocks' ),
			'section'           => 'clean_blocks_layout_options',
			'type'              => 'select',
			'choices'           => array(
				'right-sidebar'         => esc_html__( 'Right Sidebar ( Content, Primary Sidebar )', 'clean-blocks' ),
				'no-sidebar-full-width' => esc_html__( 'No Sidebar: Full Width', 'clean-blocks' ),
			),
		)
	);

	/* Homepage/Archive Layout */
	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_homepage_archive_layout',
			'default'           => 'no-sidebar-full-width',
			'sanitize_callback' => 'clean_blocks_sanitize_select',
			'label'             => esc_html__( 'Homepage/Archive Layout', 'clean-blocks' ),
			'section'           => 'clean_blocks_layout_options',
			'type'              => 'select',
			'choices'           => array(
				'right-sidebar'         => esc_html__( 'Right Sidebar ( Content, Primary Sidebar )', 'clean-blocks' ),
				'no-sidebar-full-width' => esc_html__( 'No Sidebar: Full Width', 'clean-blocks' ),
			),
		)
	);

	/* Single Page/Post Image Layout */
	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_single_layout',
			'default'           => 'disabled',
			'sanitize_callback' => 'clean_blocks_sanitize_select',
			'label'             => esc_html__( 'Single Page/Post Image Layout', 'clean-blocks' ),
			'section'           => 'clean_blocks_layout_options',
			'type'              => 'select',
			'choices'           => array(
				'disabled'             => esc_html__( 'Disabled', 'clean-blocks' ),
				'post-thumbnail'       => esc_html__( 'Post Thumbnail', 'clean-blocks' ),
			),
		)
	);

	// Excerpt Options.
	$wp_customize->add_section( 'clean_blocks_excerpt_options', array(
		'panel'     => 'clean_blocks_theme_options',
		'title'     => esc_html__( 'Excerpt Options', 'clean-blocks' ),
	) );

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_excerpt_length',
			'default'           => '20',
			'sanitize_callback' => 'absint',
			'description' => esc_html__( 'Excerpt length. Default is 20 words', 'clean-blocks' ),
			'input_attrs' => array(
				'min'   => 10,
				'max'   => 200,
				'step'  => 5,
				'style' => 'width: 60px;',
			),
			'label'    => esc_html__( 'Excerpt Length (words)', 'clean-blocks' ),
			'section'  => 'clean_blocks_excerpt_options',
			'type'     => 'number',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_excerpt_more_text',
			'default'           => esc_html__( 'Read More', 'clean-blocks' ),
			'sanitize_callback' => 'sanitize_text_field',
			'label'             => esc_html__( 'Read More Text', 'clean-blocks' ),
			'section'           => 'clean_blocks_excerpt_options',
			'type'              => 'text',
		)
	);

	// Excerpt Options.
	$wp_customize->add_section( 'clean_blocks_search_options', array(
		'panel'     => 'clean_blocks_theme_options',
		'title'     => esc_html__( 'Search Options', 'clean-blocks' ),
	) );

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_search_text',
			'default'           => esc_html__( 'Search', 'clean-blocks' ),
			'sanitize_callback' => 'sanitize_text_field',
			'label'             => esc_html__( 'Search Text', 'clean-blocks' ),
			'section'           => 'clean_blocks_search_options',
			'type'              => 'text',
		)
	);

	// Homepage / Frontpage Options.
	$wp_customize->add_section( 'clean_blocks_homepage_options', array(
		'description' => esc_html__( 'Only posts that belong to the categories selected here will be displayed on the front page', 'clean-blocks' ),
		'panel'       => 'clean_blocks_theme_options',
		'title'       => esc_html__( 'Homepage / Frontpage Options', 'clean-blocks' ),
	) );

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_front_page_category',
			'sanitize_callback' => 'clean_blocks_sanitize_category_list',
			'custom_control'    => 'Clean_Blocks_Multi_Cat_Control',
			'label'             => esc_html__( 'Categories', 'clean-blocks' ),
			'section'           => 'clean_blocks_homepage_options',
			'type'              => 'dropdown-categories',
		)
	);

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_recent_posts_heading',
			'sanitize_callback' => 'sanitize_text_field',
			'default'           => esc_html__( 'Blog', 'clean-blocks' ),
			'label'             => esc_html__( 'Recent Posts Heading', 'clean-blocks' ),
			'section'           => 'clean_blocks_homepage_options',
		)
	);
	
	// Pagination Options.
	$pagination_type = get_theme_mod( 'clean_blocks_pagination_type', 'default' );

	$nav_desc = '';

	/**
	* Check if navigation type is Jetpack Infinite Scroll and if it is enabled
	*/
	$nav_desc = sprintf(
		wp_kses(
			__( 'For infinite scrolling, use %1$sCatch Infinite Scroll Plugin%2$s with Infinite Scroll module Enabled.', 'clean-blocks' ),
			array(
				'a' => array(
					'href' => array(),
					'target' => array(),
				),
				'br'=> array()
			)
		),
		'<a target="_blank" href="https://wordpress.org/plugins/catch-infinite-scroll/">',
		'</a>'
	);
	
	$wp_customize->add_section( 'clean_blocks_pagination_options', array(
		'description' => $nav_desc,
		'panel'       => 'clean_blocks_theme_options',
		'title'       => esc_html__( 'Pagination Options', 'clean-blocks' ),
	) );

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_pagination_type',
			'default'           => 'default',
			'sanitize_callback' => 'clean_blocks_sanitize_select',
			'choices'           => clean_blocks_get_pagination_types(),
			'label'             => esc_html__( 'Pagination type', 'clean-blocks' ),
			'section'           => 'clean_blocks_pagination_options',
			'type'              => 'select',
		)
	);

	/* Scrollup Options */
	$wp_customize->add_section( 'clean_blocks_scrollup', array(
		'panel'    => 'clean_blocks_theme_options',
		'title'    => esc_html__( 'Scrollup Options', 'clean-blocks' ),
	) );

	clean_blocks_register_option( $wp_customize, array(
			'name'              => 'clean_blocks_scrollup',
			'sanitize_callback' => 'clean_blocks_sanitize_checkbox',
			'default'           => 1,
			'label'             => esc_html__( 'Scroll Up', 'clean-blocks' ),
			'section'           => 'clean_blocks_scrollup',
			'custom_control'    => 'Clean_Blocks_Toggle_Control',
		)
	);
	
}
add_action( 'customize_register', 'clean_blocks_theme_options' );