=== Clean Blocks ===
Contributors: catchthemes
Tags: blog, photography, portfolio, one-column, two-columns, right-sidebar, custom-header, custom-logo, custom-menu, editor-style, featured-image-header, featured-images, flexible-header, footer-widgets, full-width-template, rtl-language-support, sticky-post, theme-options, threaded-comments, translation-ready, block-styles, wide-blocks
Requires at least: 4.9
Tested up to: 5.3
Stable tag: 1.1.6
License: GNU General Public License v3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Clean Blocks is a simple and sophisticated, free multipurpose Gutenberg WordPress theme crafted with Gutenberg compatibility in mind.

== Description ==

Clean Blocks is a simple and sophisticated, free multipurpose Gutenberg WordPress theme crafted with Gutenberg compatibility in mind. It supports all Gutenberg blocks and is equipped with enhanced block styles. Like its name, the theme is simple, clean yet power packed with features to create modern and highly optimized websites of any kind. This theme has a gorgeous design that will be suitable for different types of websites. Amazing features like Featured Content, Featured Slider, Hero Content, Portfolio, Service, Testimonials, and more have been added to Clean Blocks. The process of realizing your ideas takes a minimal amount of steps, making sure you get the most out of Clean Blocks without investing too much time and effort. It also supports all devices with modern browsers. To top it all, the theme is translation-ready. And if you want advanced features for your website, check out the premium version. To top it all, the theme is translation ready. For any technical issue, please post in our support forum at https://catchthemes.com/support/ For more information, check out Theme Instructions at https://catchthemes.com/themes/clean-blocks/#theme-instructions and Demo at https://catchthemes.com/demo/clean-blocks/

For more information about Clean Blocks please go to https://catchthemes.com/themes/clean-blocks/

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Clean Blocks in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
4. Go to https://catchthemes.com/themes/clean-blocks/#theme-instructions/ for a guide on how to customize this theme.
5. Navigate to Appearance -> Customize in your admin panel and customize to taste.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Theme supports all plugins from catchplugins.com.

= Where can I find theme documentation? =

You can check our Theme Instructions at https://catchthemes.com/themes/clean-blocks/ in the Theme Instructions Tab.

= Where can I find theme demo? =

You can check our Theme Demo at https://www.catchthemes.com/demo/clean-blocks/

= More FAQ =

For more FAQs, visit https://catchthemes.com/frequently-asked-questions/

== Copyright ==

Clean Blocks WordPress Theme, Copyright 2012-2019 Catchthemes.com
Clean Blocks is distributed under the terms of the GNU GPL

== Resources ==
* Based on Underscores http://underscores.me/, (C) 2012-2019 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

* JS Files
	HTML5 Shiv v3.7.0, Copyright 2014 Alexander Farkas
	License: MIT/GPL2
	Source: https://github.com/aFarkas/html5shiv

	jQuery Cycle 2, Copyright (c) 2014 M. Alsup
	License: MIT/GPL
    http://jquery.malsup.com/cycle2/

	jquery-match-height master by @liabru
	License: MIT
	http://brm.io/jquery-match-height/

* Images
	All images are licensed under Creative Commons Zero (CC0) license

	Header Image and Screenshot main image
		* https://stocksnap.io/photo/QXVSHD5FLF
		* https://stocksnap.io/photo/UB2UXMASSW
		* https://stocksnap.io/photo/T4F8EDTB2E
		* https://stocksnap.io/photo/N7FUTW0RY5

* PHP Libraries
	TGM-Plugin-Activation, Copyright (c) 2011, Thomas Griffin
	License: GPL-2.0+
	http://tgmpluginactivation.com/

* Icons
	Font Awesome icons, Copyright Dave Gandy
	License: SIL Open Font License, version 1.1.
	Source: http://fontawesome.io/

	All other images are self shot image by (CatchThemes.com) and released under same license as theme

== Changelog ==

= 1.1.6 (Released: February 07, 2020) =
* Bug Fixed: Sidebar issue when main sidebar inactive

= 1.1.5 (Released: January 17, 2019) =
* Removed: Demo import code

= 1.1.4 (Released: November 22, 2019) =
* Bug Fixed: Multi Category Sanitization issue

= 1.1.3 (Released: August 12, 2019) =
* Bug Fixed: Keyboard navigation accessibility issue

= 1.1.2 (Released: July 25, 2019) =
* Bug Fixed: error.log function removed

= 1.1.1 (Released: July 15, 2019) =
* Added: Catch Themes Demo import plugin support

= 1.1 (Released: May 04, 2019) =
* Added: Css file style.css.map
* Bug Fixed: Button border removed form load more infinite scroll
* Bug Fixed: Comment section border color changed
* Bug Fixed: Slider responsive fix
* Bug Fixed: Promotional headline alignment
* Bug Fixed: Link color fix
* Bug Fixed: IE menu fix

= 1.0 (Released: April 16, 2019) =
* Initial release in CatchThemes.com
* Submitted to WordPress.org
