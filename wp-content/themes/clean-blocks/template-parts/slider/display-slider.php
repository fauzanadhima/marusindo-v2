<?php
/**
 * The template used for displaying slider
 *
 * @package Clean_Blocks
 */
?>

<?php
/**
 * clean_blocks_slider hook
 * @hooked clean_blocks_featured_slider - 10
 */
do_action( 'clean_blocks_slider' );
