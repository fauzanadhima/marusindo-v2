<?php
/**
 * The template used for displaying service
 *
 * @package Clean_Blocks
 */
?>

<?php
/**
 * clean_blocks_service hook
 * @hooked clean_blocks_service_display - 10
 */
do_action( 'clean_blocks_service' );
