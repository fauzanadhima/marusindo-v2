<?php
/**
 * The template part for displaying content
 *
 * @package Clean_Blocks
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post-wrapper">
		

		<div class="entry-container">
			<header class="entry-header">
				<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
					<span class="sticky-post"><?php esc_html_e( 'Featured', 'clean-blocks' ); ?></span>
				<?php endif; ?>

				<?php clean_blocks_post_thumbnail(); ?>

				<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

				<?php echo clean_blocks_entry_header(); ?>

			</header><!-- .entry-header -->
				<div class="entry-summary">
					<?php the_excerpt(); ?>
					
					<?php 
						clean_blocks_edit_link();
					?>
				</div><!-- .entry-summary -->
			<?php
			echo clean_blocks_entry_footer(); ?>
		</div><!-- .entry-container -->
	</div><!-- .hentry-inner -->
</article><!-- #post-## -->
