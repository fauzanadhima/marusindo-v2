<?php
/**
 * The template used for displaying projects on index view
 *
 * @package Clean_Blocks
 */
?>

<?php
$show_meta    = get_theme_mod( 'clean_blocks_portfolio_meta_show', 'show-meta' );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> class="hentry">
	<div class="hentry-inner">
		<div class="portfolio-thumbnail post-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php
				// Output the featured image.
				if ( has_post_thumbnail() ) {

					$thumbnail = 'clean-blocks-featured';
					
					the_post_thumbnail( $thumbnail );
				} else {
					echo '<img src="' . trailingslashit( esc_url( get_template_directory_uri() ) ) . 'assets/images/no-thumb.jpg"/>';
				}
				?>

				<span><?php echo clean_blocks_get_svg( array( 'icon' => 'search' ) ); ?></span>
			</a>
		</div><!-- .portfolio-thumbnail -->

		<div class="entry-container">
			<div class="inner-wrap">
				<header class="entry-header portfolio-entry-header">
					<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>

					<?php echo clean_blocks_entry_category_date(); ?>

				</header>
			</div><!-- .inner-wrap -->
		</div><!-- .entry-container -->
	</div><!-- .hentry-inner -->
</article>
