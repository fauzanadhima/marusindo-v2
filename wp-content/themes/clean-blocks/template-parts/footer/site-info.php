<?php
/**
 * The template used for displaying credits
 *
 * @package Clean_Blocks
 */
?>

<?php
/**
 * clean_blocks_credits hook
 * @hooked clean_blocks_footer_content - 10
 */
do_action( 'clean_blocks_credits' );
