<?php
/**
 * Displays Primary Navigation
 *
 * @package Clean_Blocks
 */
?>
	<button id="primary-menu-toggle" class="menu-primary-toggle menu-toggle" aria-controls="primary-menu" aria-expanded="false">
		<?php
		echo clean_blocks_get_svg( array( 'icon' => 'bars' ) );
		echo clean_blocks_get_svg( array( 'icon' => 'close' ) );
		echo '<span class="menu-label-prefix">'. esc_attr__( 'Primary ', 'clean-blocks' ) . '</span>'; ?>
			<span class="menu-label">Menu</span>
	</button>

	<div id="site-header-menu" class="site-primary-menu">
			<?php if ( has_nav_menu( 'menu-1' ) ) : ?>
				<nav id="site-primary-navigation" class="main-navigation site-navigation custom-primary-menu" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'clean-blocks' ); ?>">
					<?php wp_nav_menu( array(
						'theme_location'	=> 'menu-1',
						'container_class'	=> 'primary-menu-container',
						'menu_class'		=> 'primary-menu',
					) ); ?>
				</nav><!-- #site-primary-navigation.custom-primary-menu -->
			<?php else : ?>
				<nav id="site-primary-navigation" class="main-navigation site-navigation default-page-menu" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'clean-blocks' ); ?>">
					<?php wp_page_menu(
						array(
							'menu_class' => 'primary-menu-container',
							'before'     => '<ul id="primary-page-menu" class="primary-menu">',
							'after'      => '</ul>',
						)
					); ?>
				</nav><!-- #site-primary-navigation.default-page-menu -->
			<?php endif; ?>

		<div class="primary-search-wrapper">
			<div id="search-social-container-right" class="search-social-container">
				<button id="search-toggle-right" class="menu-search-toggle menu-toggle search-toggle">
					<?php 
						echo clean_blocks_get_svg( array( 'icon' => 'search' ) );
						echo clean_blocks_get_svg( array( 'icon' => 'close' ) );
					?>
					<span class="screen-reader-text"><?php esc_html_e( 'Search', 'clean-blocks' ); ?></span>
				</button>

				<div id="search-container" class="search-container">
					<?php get_search_form(); ?>
				</div><!-- #search-container -->
			</div><!-- #search-social-container -->
		</div><!-- .primary-search-wrapper -->
	</div><!-- #site-header-menu -->
