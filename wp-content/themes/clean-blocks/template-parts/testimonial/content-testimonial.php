<?php
/**
 * The template used for displaying testimonial on front page
 *
 * @package Clean_Blocks
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="hentry-inner">
		<?php if ( has_post_thumbnail() ) : ?>
			<div class="testimonial-thumbnail post-thumbnail">
				<?php the_post_thumbnail( 'clean-blocks-testimonial' ); ?>
			</div>
		<?php endif; ?>
		
		<div class="entry-container">
				<div class="entry-content">
					<?php the_excerpt(); ?>
				</div>
			<?php $position = get_post_meta( get_the_id(), 'ect_testimonial_position', true ); ?>

			<?php if ( $position ) : ?>
					<header class="entry-header">
						<?php 
							the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
							
						?>
						<?php if ( $position ) : ?>
							<p class="entry-meta"><span class="position">
								<?php echo esc_html( $position ); ?></span>
							</p>
						<?php endif; ?>
					</header>
			<?php endif;?>
		</div><!-- .entry-container -->
	</div><!-- .hentry-inner -->
</article>
