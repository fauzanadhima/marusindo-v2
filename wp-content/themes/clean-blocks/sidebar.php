<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package Clean_Blocks
 */
?>

<?php
$clean_blocks_layout = clean_blocks_get_theme_layout();

// Bail early if no sidebar layout is selected.
if ( 'no-sidebar' === $clean_blocks_layout || 'no-sidebar-full-width' === $clean_blocks_layout ) {
	return;
}

$sidebar = clean_blocks_get_sidebar_id();

if ( '' === $sidebar ) {
    return;
}
?>

<aside id="secondary" class="sidebar widget-area" role="complementary">
	<?php dynamic_sidebar( $sidebar ); ?>
</aside><!-- .sidebar .widget-area -->
